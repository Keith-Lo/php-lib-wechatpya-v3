#Wechat Pay API V3

Because Wechat Pay only provided a Guzzle Middleware(https://github.com/wechatpay-apiv3/wechatpay-guzzle-middleware) for PHP SDK, I wrote this code for a more native (by using curl) PHP implementation.


## API Documentation

Please check Wechat Pay V3 for the official documentation.