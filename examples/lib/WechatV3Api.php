<?php
/**
 * Wechat API V3 (PHP)
 * 
 * Please notice that this is an non official API SDK
 * Official PHP SDK only offered Guzzle plugin (https://github.com/wechatpay-apiv3/wechatpay-guzzle-middleware)
 * 
 * For Version 3 official documentation please check out (https://pay.weixin.qq.com/wiki/doc/api/micropay.php?chapter=1_1) 
 * 
 * @author Keith Lo
 * @date 2019-12-09
 */
class WechatPayV3{

// # Configuration, please replace by your own configuration : BEGIN //
    public $host = "api.mch.weixin.qq.com";
    public $merchantId = "";
    public $serialNumber = "";
    public $keyDir = '';
    public $keyFile = 'apiclient_key.pem';
    public $authorization = "WECHATPAY2-SHA256-RSA2048";
    public $timeout = 13;
// # Configuration, please replace by your own configuration : END //

    public function execute($query, $params = array())
    {
        $map = $this->getApiRequest($query);
        $httpMethod = $map['method'];
        $query = $map['query'];

        $now = time();
        $nonceStr = $this->generateNonce();
        $query = $this->getUrlQuery($query, $params);
        $signature = $this->sign($httpMethod, $query, $now, $nonceStr, $params);

        $authorization = $this->getAuthorizationToken($this->merchantId, $nonceStr, $signature, $now, $this->serialNumber);
        
        $response = $this->curl($httpMethod, $query, $params, $authorization);
        
        return json_decode($response, true);
    }

    protected function curl($httpMethod, $query, $params, $authorization)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://".$this->host.$query,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "UTF-8",
            CURLOPT_TIMEOUT => $this->timeout,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $httpMethod,
            CURLOPT_POSTFIELDS => json_encode($params),
            CURLOPT_HTTPHEADER => array(
                "Accept: application/json",
                "Accept-Language: en",
                "Authorization: ".$authorization,
                "Content-Type: application/json",
                "Host: ".$this->host,
                "User-Agent: curl/7.35.0",
            ),
        ));
   
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if( $err ){
            throw new Exception("Calling WechatPay Server Failed #: ".$err);
        }

        return $response;
    }

    protected function generateNonce()
    {
        static $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 32; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    protected function getApiRequest($query)
    {
        $map = $this->_getApiMap();
        if( !array_key_exists($query, $map) ){
            throw new Exception("API name not found. Please check your query is correct.");
        }

        return $map[$query];
    }

    protected function getAuthorizationToken($merchantId, $nonceStr, $signature, $now, $serialNumber)
    {
        $authorization = $this->authorization;
        $authParams = array(
            "mchid" => $merchantId, //Merchant ID
            "nonce_str" => $nonceStr,
            "signature" => $signature,
            "timestamp" => $now,
            "serial_no" => $serialNumber,
        );
        $temp = array();
        foreach( $authParams as $key => $value ){
            $temp[] = $key.'="'.$value.'"';
        }
        $authorization .= " ".implode(",", $temp);

        return $authorization;
    }

    protected function getUrlQuery($url, &$params)
    {
        $count = 0;
        foreach( $params as $key => $value ){
            $url = preg_replace('/\{'.$key.'\}/', $value, $url, -1, $count);
            if( $count > 0 ){
                unset($params[$key]);
            }
        }

        return $url;
    }

    protected function sign($httpMethod, $query, $now, $nonceStr, $params)
    {
        $forSign = '';
        $forSign .= $httpMethod."\n";
        $forSign .= $query."\n";
        $forSign .= $now."\n";
        $forSign .= $nonceStr."\n";
        $forSign .= ( !empty($params) ) ? json_encode($params)."\n" : "\n";

        $keys = $this->getKeyFile();

        if (!openssl_sign($forSign, $signature, $keys, 'sha256WithRSAEncryption')) {
            throw new Exception('Cannot sign');
        }
        return base64_encode($signature);
    }

    protected function getKeyFile()
    {
        $keyFile = $this->keyDir . $this->keyFile;
        if( !file_exists($keyFile) ){
            throw new Exception('Private key not found. Please check the configuration files');
        }

        return file_get_contents($keyFile);
    }

    private function _getApiMap()
    {
        return array(
            self::API_ORDER_ENQUIRY => array("method" => self::HTTP_GET, "query" => "/hk/v3/transactions/out-trade-no/{out_trade_no}?mchid={mchid}"),
        );
    }


    const API_ORDER_ENQUIRY = 'ORDER_ENQUIRY';

    const HTTP_GET = 'GET';
}